#include "Adafruit_Keypad.h" // knihovna pto rastr klavesnice
#include "Keyboard.h" // USB HID

#define KEYPRESS_LENGTH         100 // ms

const byte ROWS = 4; // rows
const byte COLS = 3; // columns
//define the symbols on the buttons of the keypads
char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};
byte rowPins[ROWS] = {2, 3, 4, 5}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {6, 7, 8}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Adafruit_Keypad customKeypad = Adafruit_Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS);

void setup() {
  Serial.begin(9600);
  customKeypad.begin();
  Keyboard.begin();

}

void loop() {
  // put your main code here, to run repeatedly:
  customKeypad.tick();

  while (customKeypad.available()) {
    keypadEvent e = customKeypad.read();
    char chr = (char)e.bit.KEY;
    Serial.print(chr);
    //Keyboard.write(chr);
    if (e.bit.EVENT == KEY_JUST_PRESSED) {
      Serial.println(" pressed");

    }
    else if (e.bit.EVENT == KEY_JUST_RELEASED) {
      Serial.println(" released");
      switch (chr) {
        case '1':
          //scena
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F1);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();


          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F12);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          break;

        case '2':
          // statements
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F2);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F12);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();
          break;

        case '3':
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F3);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F12);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          break;

        case '4':
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F4);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F12);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          break;

        case '5':
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F5);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F12);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          break;
        case '6':
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F6);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F12);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();
          break;
        case '7':
          //rouska
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F7);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();
          //unmute
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F11);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();
          break;
        case '8':
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F8);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F12);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();
          break;
        case '9':
          //zobrazeni rousky
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F9);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          //mute
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F10);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();
          break;
        case '0':
          // postrih za pouziti uvodni sceny
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F10);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F12);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();

          delay(3000);

          // navrat zpet
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_F12);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();          break;
        case '#':
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F10);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();
          break;
        case '*':
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_SHIFT);
          Keyboard.press(KEY_LEFT_ALT);
          Keyboard.press(KEY_F11);
          delay(KEYPRESS_LENGTH);
          Keyboard.releaseAll();
        default:
          // statements

          break;
      }

    }

  }

  // delay(10);
}
